#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

void separateGivenString(char* token, int sum)
{

    int num = 0;

    // Keep printing tokens while one of the
    // delimiters present in the given string
    while(token != NULL)
    {
        // Convert all tokens to integers
        num = atoi(token);

        // Print converted integer values
        printf("\nString value \"%s\" - integer value %d\n", token, num);

        sum += num;

        token = strtok(NULL, STRING_SEPARATOR);
    }

}

