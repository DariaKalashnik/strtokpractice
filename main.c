#include <stdio.h>
#include <stdlib.h>

#include "utils.h"

int main(int argc, char* argv[])
{
    int sum = 0;

    // Returns first token
    char* token = strtok(argv[1], STRING_SEPARATOR);

    printf("Tokes are:\n----------");

    separateGivenString(token, sum);

    printf("\n----------");

    // Print sum of all numbers
    printf("\nSum = %d\n", sum);

    return EXIT_SUCCESS;
}


